# Methods

Methods are written with `Please` statements. For example, to print the variable A, you would write:
```
Please print a
```