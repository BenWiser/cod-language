# Sentences

Statements in `Cod` are called sentences. They are either delimited by end of lines, or by 
full stops.

These are both valid sentences:
eg:
```
Integer a is 2
Please print a

Integer b is 2. Please print b.
```