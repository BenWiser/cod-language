# IO

Input and output are super basic. To write a value to the screen, you can use `print`. To
read a line into a variable, use the `from` operator and specify the source. `keyboard` is currently
the only source supported.

Reading a value:
```
String name is from keyboard
```

Printing a value:
```
Please print name
```