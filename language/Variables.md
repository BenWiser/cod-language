# Variables

Primitives require type identifiers in Cod. They are written in full names for clarity.
eg:
```
Integer a is 2.
```

Cod is a very verbose language. Notice that the keyword `is` was used instead of the more
traditional `=`. `is` specifies what a variable should store. Variable types are all upper case.

The primitive types are:
- `Integer`
- `Float`
- `String`
- `Boolean`

`Float` variables use commas for decimals.
eg:
```
Float f is 3,2.
```