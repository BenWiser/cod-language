#include "gtest/gtest.h"

#include <vector>
#include <string>
#include <memory>
#include "tokens/Token.h"
#include "tokens/Integer.h"
#include "tokens/Float.h"
#include "tokens/String.h"
#include "tokens/Boolean.h"
#include "tokens/Method.h"
#include "tokens/Symbol.h"
#include "tokens/Is.h"
#include "tokens/Value.hpp"
#include "tokens/End.h"
#include "Lexer.h"

TEST(Lexer, IntegerVariables)
{
    Lexer lexer;
    auto actual_tokens = lexer.lex(std::vector<std::string> {"Integer a is 2"});
    std::vector<std::shared_ptr<Token>> expected_tokens {
        std::make_shared<IntegerToken>(),
        std::make_shared<SymbolToken>("a"),
        std::make_shared<IsToken>(),
        std::make_shared<ValueToken<int>>(2),
        std::make_shared<EndToken>(),
    };

    ASSERT_EQ(expected_tokens.size(), actual_tokens.size());

    for (int i = 0; i < expected_tokens.size(); i++)
    {
        auto expected_token = expected_tokens[i].get();
        auto actual_token = actual_tokens[i].get();

        ASSERT_TRUE(*expected_token == *actual_token);
    }
}

TEST(Lexer, FloatVariables)
{
    Lexer lexer;
    auto actual_tokens = lexer.lex(std::vector<std::string> {"Float f is 2,2"});
    std::vector<std::shared_ptr<Token>> expected_tokens {
        std::make_shared<FloatToken>(),
        std::make_shared<SymbolToken>("f"),
        std::make_shared<IsToken>(),
        std::make_shared<ValueToken<float>>(2.2),
        std::make_shared<EndToken>(),
    };

    ASSERT_EQ(expected_tokens.size(), actual_tokens.size());

    for (int i = 0; i < expected_tokens.size(); i++)
    {
        auto expected_token = expected_tokens[i].get();
        auto actual_token = actual_tokens[i].get();

        ASSERT_TRUE(*expected_token == *actual_token);
    }
}

TEST(Lexer, StringVariables)
{
    Lexer lexer;
    auto actual_tokens = lexer.lex(std::vector<std::string> {"String name is ben"});
    std::vector<std::shared_ptr<Token>> expected_tokens {
        std::make_shared<StringToken>(),
        std::make_shared<SymbolToken>("name"),
        std::make_shared<IsToken>(),
        std::make_shared<ValueToken<std::string>>("ben"),
        std::make_shared<EndToken>(),
    };

    ASSERT_EQ(expected_tokens.size(), actual_tokens.size());

    for (int i = 0; i < expected_tokens.size(); i++)
    {
        auto expected_token = expected_tokens[i].get();
        auto actual_token = actual_tokens[i].get();

        ASSERT_TRUE(*expected_token == *actual_token);
    }
}

TEST(Lexer, BooleanVariables)
{
    Lexer lexer;
    auto actual_tokens = lexer.lex(std::vector<std::string> {"Boolean b is true"});
    std::vector<std::shared_ptr<Token>> expected_tokens {
        std::make_shared<BooleanToken>(),
        std::make_shared<SymbolToken>("b"),
        std::make_shared<IsToken>(),
        std::make_shared<ValueToken<bool>>(true),
        std::make_shared<EndToken>(),
    };

    ASSERT_EQ(expected_tokens.size(), actual_tokens.size());

    for (int i = 0; i < expected_tokens.size(); i++)
    {
        auto expected_token = expected_tokens[i].get();
        auto actual_token = actual_tokens[i].get();

        ASSERT_TRUE(*expected_token == *actual_token);
    }
}

TEST(Lexer, FullStops)
{
    Lexer lexer;
    auto actual_tokens = lexer.lex(std::vector<std::string> {"Boolean b is true. Boolean b is false."});
    std::vector<std::shared_ptr<Token>> expected_tokens {
        std::make_shared<BooleanToken>(),
        std::make_shared<SymbolToken>("b"),
        std::make_shared<IsToken>(),
        std::make_shared<ValueToken<bool>>(true),
        std::make_shared<EndToken>(),
        std::make_shared<BooleanToken>(),
        std::make_shared<SymbolToken>("b"),
        std::make_shared<IsToken>(),
        std::make_shared<ValueToken<bool>>(false),
        std::make_shared<EndToken>(),
        std::make_shared<EndToken>(),
    };

    ASSERT_EQ(expected_tokens.size(), actual_tokens.size());

    for (int i = 0; i < expected_tokens.size(); i++)
    {
        auto expected_token = expected_tokens[i].get();
        auto actual_token = actual_tokens[i].get();

        ASSERT_TRUE(*expected_token == *actual_token);
    }
}

TEST(Lexer, Methods)
{
    Lexer lexer;
    auto actual_tokens = lexer.lex(std::vector<std::string> {"Please print"});
    std::vector<std::shared_ptr<Token>> expected_tokens {
        std::make_shared<MethodToken>("print"),
        std::make_shared<EndToken>(),
    };

    ASSERT_EQ(expected_tokens.size(), actual_tokens.size());

    for (int i = 0; i < expected_tokens.size(); i++)
    {
        auto expected_token = expected_tokens[i].get();
        auto actual_token = actual_tokens[i].get();

        ASSERT_TRUE(*expected_token == *actual_token);
    }
}