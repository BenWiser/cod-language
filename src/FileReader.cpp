#include "FileReader.h"

#include <fstream>

std::vector<std::string> FileReader::read(std::string fileName)
{
    using std::string;

    std::ifstream file(fileName);
    std::vector<string> file_data {};

    for (string line; std::getline(file, line);)
    {
        file_data.push_back(line);
    }

    return file_data;
}