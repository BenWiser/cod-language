#include "parser/Parser.h"

#include <iostream>

#include "tokens/End.h"
#include "tokens/Variable.h"
#include "tokens/Is.h"
#include "tokens/Method.h"
#include "tokens/Symbol.h"
#include "tokens/Value.hpp"
#include "ast/ValueNode.hpp"
#include "ast/VariableNode.h"

namespace Parse
{
    using std::shared_ptr;
    using std::vector;

    void method(const shared_ptr<TreeNode>& current_node, const vector<shared_ptr<Token>>& tokens,
        uint& token_index, const MethodToken* method_token)
    {
        auto method_call = std::make_shared<TreeNode>(current_node, "method_call");
        method_call->push_node(std::make_shared<TreeNode>(current_node, method_token->getName()));

        auto parameter_token = tokens[++token_index];

        while (!dynamic_cast<EndToken*>(parameter_token.get()))
        {
            auto symbol_parameter = dynamic_cast<SymbolToken*>(parameter_token.get());

            if (symbol_parameter)
            {
                method_call->push_node(std::make_shared<VariableNode>(current_node, "symbol",
                    symbol_parameter->getName(), "Auto"));
            }

            token_index++;
            parameter_token = tokens[++token_index];
        }

        current_node->push_node(method_call);
    }

    void variable(const shared_ptr<TreeNode>& current_node, const vector<shared_ptr<Token>>& tokens,
        uint& token_index)
    {
        auto symbol = dynamic_cast<SymbolToken*>(tokens[++token_index].get());

        if (!symbol)
        {
            // TODO: Add proper error handling
            std::cerr << "[ERROR] A symbol was expect!" << std::endl;
            exit(1);
        }

        auto relation = dynamic_cast<IsToken*>(tokens[++token_index].get());

        if (!relation)
        {
            // TODO: Add proper error handling
            std::cerr << "[ERROR] An assignment was expect!" << std::endl;
            exit(1);
        }

        // TODO: Handle different types of relations
        auto relation_node = std::make_shared<TreeNode>(current_node, "assignment");

        // value_type is just provided as a hack to get the type for the value
        auto get_value = [&](std::string type_name, auto value_type) {
            auto value = dynamic_cast<ValueToken<decltype(value_type)>*>(tokens[token_index].get());

            if (!value)
            {
                return false;
            }

            auto statement = std::make_shared<TreeNode>(current_node, "statement");
            statement->push_node(std::make_shared<ValueNode<decltype(value_type)>>(relation_node, "value", value->getValue()));

            relation_node->push_node(std::make_shared<VariableNode>(relation_node, "variable", symbol->getName(), type_name));
            relation_node->push_node(statement);

            return true;
        };

        token_index++;

        auto value_assigned = get_value("Integer", 0);
        if (!value_assigned) value_assigned = get_value("Float", 0.0f);
        if (!value_assigned) value_assigned = get_value("String", std::string(""));
        if (!value_assigned) value_assigned = get_value("Boolean", false);

        if (!value_assigned)
        {
            // TODO: Add proper error handling
            std::cerr << "[ERROR] A value was expect!" << std::endl;
            exit(1);
        }

        current_node->push_node(relation_node);
    }
} // namespace Parse

std::shared_ptr<TreeNode> Parser::parse(std::vector<std::shared_ptr<Token>> tokens)
{
    std::shared_ptr<TreeNode> ast = std::make_shared<TreeNode>(nullptr, "root");
    std::shared_ptr<TreeNode> current_node = nullptr;

    for (uint i = 0; i < tokens.size(); i ++)
    {
        auto token = tokens[i];

        if (current_node == nullptr)
        {
            current_node = std::make_shared<TreeNode>(ast, "sentence");
            ast->push_node(current_node);
        }

        auto method_token = dynamic_cast<MethodToken*>(token.get());
        if (method_token)
        {
            Parse::method(current_node, tokens, i, method_token);
        }

        if (dynamic_cast<VariableToken*>(token.get()))
        {
            Parse::variable(current_node, tokens, i);
        }

        if (dynamic_cast<EndToken*>(token.get()))
        {
            current_node = current_node->getParent();
        }
    }

    return ast;
}