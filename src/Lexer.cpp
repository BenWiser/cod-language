#include "Lexer.h"

#include <sstream>
#include <iostream>
#include <typeinfo>
#include <algorithm>

#include "tokens/Value.hpp"
#include "tokens/Integer.h"
#include "tokens/Float.h"
#include "tokens/String.h"
#include "tokens/Boolean.h"
#include "tokens/Symbol.h"
#include "tokens/Method.h"
#include "tokens/End.h"
#include "tokens/Is.h"

std::vector<std::shared_ptr<Token>> Lexer::lex(std::vector<std::string> sourceCode)
{
    using std::getline;
    using std::stringstream;
    using std::string;

    std::vector<std::shared_ptr<Token>> tokens {};

    for (string line: sourceCode)
    {
        bool is_assignment = false;
        bool is_method = false;
        VariableToken* variable_type = nullptr;

        stringstream line_stream { line };
        for (string word; getline(line_stream, word, ' ');)
        {
            auto has_fullstop = word.find(".") != string::npos;

            if (has_fullstop)
            {
                word.erase(std::remove(word.begin(), word.end(), '.'), word.end());
            }

            if (word == "Integer")
            {
                tokens.push_back(std::make_shared<IntegerToken>());
                variable_type = new IntegerToken();
            }
            else if (word == "Float")
            {
                tokens.push_back(std::make_shared<FloatToken>());
                variable_type = new FloatToken();
            }
            else if (word == "String")
            {
                tokens.push_back(std::make_shared<StringToken>());
                variable_type = new StringToken();
            }
            else if (word == "Boolean")
            {
                tokens.push_back(std::make_shared<BooleanToken>());
                variable_type = new BooleanToken();
            }
            else if (word == "is")
            {
                tokens.push_back(std::make_shared<IsToken>());
                is_assignment = true;
            }
            else if (is_assignment && variable_type != nullptr)
            {
                if (dynamic_cast<IntegerToken*>(variable_type))
                {
                    tokens.push_back(std::make_shared<ValueToken<int>>(std::stoi(word)));
                }
                else if (dynamic_cast<FloatToken*>(variable_type))
                {
                    std::replace(word.begin(), word.end(), ',', '.');
                    tokens.push_back(std::make_shared<ValueToken<float>>(std::stof(word)));
                }
                else if (dynamic_cast<StringToken*>(variable_type))
                {
                    tokens.push_back(std::make_shared<ValueToken<string>>(word));
                }
                else if (dynamic_cast<BooleanToken*>(variable_type))
                {
                    tokens.push_back(std::make_shared<ValueToken<bool>>(word == "true"));
                }

                delete variable_type;
                variable_type = nullptr;
                is_assignment = false;
            }
            else if (word == "Please")
            {
                is_method = true;
            }
            else if (is_method)
            {
                tokens.push_back(std::make_shared<MethodToken>(word));
                is_method = false;
            }
            else if (word.size())
            {
                tokens.push_back(std::make_shared<SymbolToken>(word));
            }

            if (has_fullstop)
            {
                tokens.push_back(std::make_shared<EndToken>());
            }
        }

        tokens.push_back(std::make_shared<EndToken>());

        if (variable_type != nullptr)
        {
            // TODO: Report error
            delete variable_type;
        }

        if (is_assignment)
        {
            // TODO: Report error
        }
    }

    return tokens;
}