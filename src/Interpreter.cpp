#include "Interpreter.h"

#include <iostream>
#include <string>
#include <functional>
#include <unordered_map>
#include "ast/VariableNode.h"
#include "ast/ValueNode.hpp"

namespace Interpret
{
using std::string;
using std::unordered_map;

struct State
{
    unordered_map<string, string> variables;

    unordered_map<string, int> integer_variables;
    unordered_map<string, float> float_variables;
    unordered_map<string, string> string_variables;
    unordered_map<string, bool> bool_variables;

    unordered_map<
        string,
        std::function<std::shared_ptr<TreeNode>(const std::vector<std::shared_ptr<TreeNode>>& method_call)>
    > methods;
};

void standard_library(State& state)
{
    state.methods["print"] = [&](const std::vector<std::shared_ptr<TreeNode>>& method_call) {
        for (int i = 1; i < method_call.size(); i++)
        {
            // TODO: Handle value parameters as well
            auto parameter = static_cast<VariableNode*>(method_call[i].get());
            if (!state.variables.count(parameter->getVariableName()))
            {
                // TODO: Add error handling
                std::cerr << "Variable [" << parameter->getVariableName() << "] does not exist" << std::endl;
                exit(1);
            }
            auto type = state.variables[parameter->getVariableName()];
            if (type == "Integer") std::cout << state.integer_variables[parameter->getVariableName()];
            if (type == "Float") std::cout << state.float_variables[parameter->getVariableName()];
            if (type == "String") std::cout << state.string_variables[parameter->getVariableName()];
            if (type == "Boolean") std::cout << state.bool_variables[parameter->getVariableName()];
        }

        std::cout << std::endl;

        return nullptr;
    };
}

std::shared_ptr<TreeNode> interpret_node(State& state, std::shared_ptr<TreeNode>& node)
{
    for (std::shared_ptr<TreeNode> child: node->getChildren())
    {
        if (child->is("sentence"))
        {
            Interpret::interpret_node(state, child);
        }
        else if (child->is("statement"))
        {
            return Interpret::interpret_node(state, child);
        }
        else if (child->is("value"))
        {
            return child;
        }
        else if (child->is("assignment"))
        {
            auto variable = static_cast<VariableNode*>(child->getChildren()[0].get());
            auto variable_name = variable->getVariableName();

            if (state.variables.count(variable_name) && state.variables[variable_name] != variable->getType())
            {
                // TODO: Add error handling
                std::cerr << "Variable [" << variable_name << "] has already been defined as type ["
                    << state.variables[variable_name] << "]" << std::endl;
                exit(1);
            }

            state.variables[variable_name] = variable->getType();

            auto result = interpret_node(state, child->getChildren()[1]);

            if (variable->isType("Integer"))
            {
                auto value = static_cast<ValueNode<int>*>(result.get());
                state.integer_variables[variable_name] = value->getValue();
            }
            else if (variable->isType("Float"))
            {
                auto value = static_cast<ValueNode<float>*>(result.get());
                state.float_variables[variable_name] = value->getValue();
            }
            else if (variable->isType("String"))
            {
                auto value = static_cast<ValueNode<std::string>*>(result.get());
                state.string_variables[variable_name] = value->getValue();
            }
            else if (variable->isType("Boolean"))
            {
                auto value = static_cast<ValueNode<bool>*>(result.get());
                state.bool_variables[variable_name] = value->getValue();
            }
        }
        else if (child->is("method_call"))
        {
            auto name = static_cast<TreeNode*>(child->getChildren()[0].get())->getName();

            if (!state.methods.count(name))
            {
                // TODO: Error reporting
                std::cerr << "The method [" << name << "] does not exist" << std::endl;
                exit(1);
            }

            state.methods[name](child->getChildren());
        }
    }

    return nullptr;
}
} // Interpret

void Interpreter::interpret(std::shared_ptr<TreeNode> ast)
{
    Interpret::State state;

    Interpret::standard_library(state);
    
    Interpret::interpret_node(state, ast);
}