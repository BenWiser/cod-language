#include <iostream>

#include "FileReader.h"
#include "Interpreter.h"
#include "Lexer.h"
#include "parser/Parser.h"

int main(int argc, char* argv[])
{
    if (argc < 2)
    {
        std::cerr << "Please provide the .cod file path" << std::endl;
        return 1;
    }

    auto source_code = FileReader::read(argv[1]);

    Lexer lexer;
    Parser parser;
    Interpreter interpreter;

    auto tokens = lexer.lex(source_code);
    auto ast = parser.parse(tokens);

    interpreter.interpret(ast);

    return 0;
}