#include "ast/TreeNode.h"

TreeNode::TreeNode(std::shared_ptr<TreeNode> parent, std::string name): _parent(parent), _nodes {}, _name(name)
{
}

void TreeNode::push_node(std::shared_ptr<TreeNode> node)
{
    _nodes.push_back(node);
}

bool TreeNode::is(std::string name)
{
    return name == _name;
}

std::string TreeNode::getName() const
{
    return _name;
}

std::shared_ptr<TreeNode> TreeNode::getParent() const
{
    return _parent;
}

std::vector<std::shared_ptr<TreeNode>> TreeNode::getChildren()
{
    return _nodes;
}