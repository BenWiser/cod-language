#include "ast/VariableNode.h"

VariableNode::VariableNode(std::shared_ptr<TreeNode> parent, std::string name, std::string variableName, std::string type):
    TreeNode(parent, name), _variableName(variableName), _type(type)
{
}

std::string VariableNode::getVariableName() const
{
    return _variableName;
}

bool VariableNode::isType(std::string type)
{
    return _type == type;
}

std::string VariableNode::getType() const
{
    return _type;
}