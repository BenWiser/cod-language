#include "tokens/Method.h"

MethodToken::MethodToken(std::string name): _name(name)
{
}

bool MethodToken::is(std::string name)
{
    return _name == name;
}

std::string MethodToken::getName() const
{
    return _name;
}

bool MethodToken::operator==(const Token& other)
{
    auto other_method = dynamic_cast<const MethodToken*>(&other);

    if (other_method)
    {
        return is(other_method->getName());
    }
    return false;
}