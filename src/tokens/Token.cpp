#include "tokens/Token.h"

std::ostream& operator<<(std::ostream& stream, const Token& token)
{
    return stream << typeid(token).name();
}

bool Token::operator==(const Token& other)
{
    return typeid(*this).name() == typeid(other).name();
}
