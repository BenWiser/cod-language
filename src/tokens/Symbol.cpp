#include "tokens/Symbol.h"

SymbolToken::SymbolToken(std::string name): _name(name)
{
}

std::string SymbolToken::getName() const
{
    return _name;
}

bool SymbolToken::operator==(const Token& other)
{
    auto other_symbol = dynamic_cast<const SymbolToken*>(&other);

    if (other_symbol)
    {
        return _name == other_symbol->getName();
    }
    return false;
}