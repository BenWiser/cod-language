# Cod

This is a little throw away project to learn about developing a programming language.

My goal is to write a simple programming language that is very readable. This language is procedural
and makes use of the Cod interpreter.

To setup this project, run:
- `git submodule update --init`
- `cmake .`
- `make`
- `./cod`