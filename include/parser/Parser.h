#ifndef _PARSER_H_
#define _PARSER_H_

#include <vector>
#include <memory>
#include "ast/TreeNode.h"
#include "tokens/Token.h"

class Parser
{
public:
    std::shared_ptr<TreeNode> parse(std::vector<std::shared_ptr<Token>> tokens);
};

#endif // _PARSER_H_