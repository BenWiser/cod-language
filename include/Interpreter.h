#ifndef _INTERPRETER_H_
#define _INTERPRETER_H_

#include <memory>
#include "ast/TreeNode.h"

class Interpreter
{
public:
    void interpret(std::shared_ptr<TreeNode> ast);
};

#endif // _INTERPRETER_H_