#ifndef _SYMBOL_H_
#define _SYMBOL_H_

#include <string>

#include "tokens/Token.h"

class SymbolToken: public Token
{
public:
    SymbolToken(std::string name);
    std::string getName() const;
    bool operator==(const Token& other);
private:
    const std::string _name;
};

#endif // _SYMBOL_H_