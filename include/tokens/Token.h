#ifndef _TOKEN_H_
#define _TOKEN_H_

#include <ostream>

class Token
{
public:
    virtual ~Token() = default;
    friend std::ostream& operator<<(std::ostream& stream, const Token& token);
    virtual bool operator==(const Token& other);
};

#endif // _TOKEN_H_