#ifndef _METHOD_TOKEN_H_
#define _METHOD_TOKEN_H_

#include "tokens/Token.h"

#include <string>
#include <vector>
#include <memory>

class MethodToken: public Token
{
public:
    MethodToken(std::string name);

    bool is(std::string name);
    std::string getName() const;
    bool operator==(const Token& other);
private:
    const std::string _name;
};

#endif // _METHOD_TOKEN_H_