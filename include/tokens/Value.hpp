#ifndef _VALUE_H_
#define _VALUE_H_

#include "tokens/Token.h"

template <typename T>
class ValueToken: public Token
{
public:
    ValueToken(T value): _value(value) {}

    const T getValue()
    {
        return _value;
    }

    virtual bool operator==(const Token& other)
    {
        auto other_value = dynamic_cast<const ValueToken<T>*>(&other);

        if (other_value)
        {
            return getValue() == other_value->_value;
        }

        return false;
    }

private:
    const T _value;
};

#endif // _VALUE_H_