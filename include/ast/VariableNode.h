#ifndef _VARIABLE_NODE_H_
#define _VARIABLE_NODE_H_

#include <string>
#include <memory>
#include "TreeNode.h"

class VariableNode: public TreeNode
{
public:
    VariableNode(std::shared_ptr<TreeNode> parent, std::string name, std::string variableName, std::string type);

    std::string getVariableName() const;
    bool isType(std::string type);
    std::string getType() const;
private:
    const std::string _variableName;
    const std::string _type;
};

#endif // _VARIABLE_NODE_H_