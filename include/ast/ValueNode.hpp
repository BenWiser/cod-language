#ifndef _VALUE_NODE_H_
#define _VALUE_NODE_H_

template<typename T>
class ValueNode: public TreeNode
{
public:
    ValueNode(std::shared_ptr<TreeNode> parent, std::string name, T value): TreeNode(parent, name),
        _value(value)
    {
    }

    T getValue() const
    {
        return _value;
    }

private:
    const T _value;
};

#endif // _VALUE_NODE_H_