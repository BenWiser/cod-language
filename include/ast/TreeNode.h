#ifndef _TREE_NODE_H_
#define _TREE_NODE_H_

#include <vector>
#include <memory>
#include <string>

class TreeNode
{
public:
    TreeNode(std::shared_ptr<TreeNode> parent, std::string name);
    void push_node(std::shared_ptr<TreeNode> node);
    bool is(std::string name);
    std::string getName() const;
    std::shared_ptr<TreeNode> getParent() const;
    std::vector<std::shared_ptr<TreeNode>> getChildren();

private:
    const std::shared_ptr<TreeNode> _parent;
    std::vector<std::shared_ptr<TreeNode>> _nodes;
    const std::string _name;
};

#endif // _TREE_NODE_H_