#ifndef _LEXER_H_
#define _LEXER_H_

#include <vector>
#include <memory>
#include "tokens/Token.h"
#include "string"

class Lexer
{
public:
    std::vector<std::shared_ptr<Token>> lex(std::vector<std::string> sourceCode);
};

#endif // _LEXER_H_