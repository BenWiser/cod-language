#ifndef _FILE_READER_H_
#define _FILE_READER_H_

#include <vector>
#include <string>

namespace FileReader
{
    using std::string;

    std::vector<string> read(string fileName);
} // namespace FileReader

#endif // _FILE_READER_H_